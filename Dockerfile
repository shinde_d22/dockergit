FROM tomcat:8.0.43-jre8
MAINTAINER deepakshindester
COPY ./target/docker.war /usr/local/tomcat/webapps/docker.war
EXPOSE 8080
CMD ["catalina.sh", "run"]